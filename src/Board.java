public class Board {

	private static int SIDE = 10;
	private static int PLAYER_NO = 2;
	private Piece[][] board = new Piece[SIDE][SIDE];
	// private List<Space> emptySpace= new LinkedList<Space>();
	private Player[] players = new Player[PLAYER_NO];
	private int currPlayer;

	public Board(Colour[] colours) {
		for (int i = 0; i < players.length; i++) {
			players[i] = new Player(colours[i]);
		}
		this.currPlayer = 0;
		for (int i = 0; i < SIDE; i++) {
			for (int j = 0; j < SIDE; j++) {
				board[i][j] = new Piece(null);
			}
		}
		System.out.println(players[currPlayer].getPiece().getColour());
	}

	public void switchPlayer() {
		currPlayer = (currPlayer + 1) % PLAYER_NO;
	}

	public boolean existValidMove() {
		// for all empty spaces
		//
		return true;
	}

	public boolean isValidMove(int xCoord, int yCoord) {
		return board[xCoord][yCoord].getColour() == null;
	}

	public void placePiece(int xCoord, int yCoord) {
		if (isValidMove(xCoord, yCoord)) {
			board[xCoord][yCoord].setColour(players[currPlayer].getPiece().getColour());
			reversePieces(xCoord, yCoord);
		 }
	}

	public void reversePieces(int xCoord, int yCoord) {
		checkHorizontally(xCoord, yCoord);
		checkVertically(xCoord, yCoord);
		checkDiagonally(xCoord, yCoord);
	}

	private void checkHorizontally(int xCoord, int yCoord) {
		Colour colour = board[xCoord][yCoord].getColour();
		for (int i = yCoord + 1; i < SIDE; i++) {
			if (colour.equals(board[xCoord][i].getColour())) {
				for (int count = yCoord; count <= i; count++) {
					board[xCoord][count].setColour(colour);
				}
				break;
			}
		}

		for (int i = yCoord - 1; i >= 0; i--) {
			if (colour.equals(board[xCoord][i].getColour())) {
				for (int count = yCoord; count >= i; count--) {
					board[xCoord][count].setColour(colour);
				}
				break;
			}
		}

	}

	private void checkVertically(int xCoord, int yCoord) {
		Colour colour = board[xCoord][yCoord].getColour();
		for (int i = xCoord + 1; i < SIDE; i++) {
			if (colour.equals(board[i][yCoord].getColour())) {
				for (int count = xCoord; count <= i; count++) {
					board[count][yCoord].setColour(colour);
				}
				break;
			}
		}

		for (int i = xCoord - 1; i >= 0; i--) {
			if (colour.equals(board[i][yCoord].getColour())) {
				for (int count = yCoord; count >= i; count--) {
					board[count][yCoord].setColour(colour);
				}
				break;
			}
		}

	}

	private void checkDiagonally(int xCoord, int yCoord) {
		int i = xCoord;
		int j = yCoord;
		Colour colour = board[xCoord][yCoord].getColour();
		while ((i++ < SIDE - 1) && (j++ < SIDE - 1)) {
			if (colour.equals(board[i][j].getColour())) {
				while(i-- > xCoord && j-- > yCoord){
					board[i][j].setColour(colour);
				}
				break;
			}
		}
		i = xCoord;
		j = yCoord;
		while (i-- > 0 && j-- > 0) {
			if (colour.equals(board[i][j].getColour())) {
				while(i++ < xCoord && j++ < yCoord){
					board[i][j].setColour(colour);
				}
				break;
			}
		}
		i = xCoord;
		j = yCoord;
		while (i++ < SIDE - 1 && j-- > 0) {
			if (colour.equals(board[i][j].getColour())) {
				while(i-- > xCoord && j++ < yCoord){
					board[i][j].setColour(colour);
				}
				break;
			}
		}
		i = xCoord;
		j = yCoord;
		while (i-- > 0 && j++ < SIDE - 1) {
			if (colour.equals(board[i][j].getColour())) {
				while(i++ < xCoord && j-- > yCoord){
					board[i][j].setColour(colour);
				}
				break;
			}
		}
	}

	public void display() {
		for (int i = 0; i < SIDE; i++) {
			for (int j = 0; j < SIDE; j++) {
				System.out.print(board[i][j].toString() + "     ");
			}
			System.out.println("");
		}
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");

	}

}
