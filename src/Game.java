
public class Game {

	
	public static void main(String[] args) {
		Colour[] colours = new Colour[2];
		colours[0] = Colour.Black;
		colours[1] = Colour.White; 
		Board board = new Board(colours);
		board.display();
		board.placePiece(0,0);
		board.placePiece(9,9);
		board.display();
		board.placePiece(0, 9);
		board.display();
		board.placePiece(3, 5);
		board.display();
	}

}
