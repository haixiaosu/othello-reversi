
public class Player {

	private int pieceNo;
	private final Piece piece;
	
	public Player(Colour colour){
		this.pieceNo = 0;
		this.piece = new Piece(colour);
	}
	
	public int getPieceNo() {
		return this.pieceNo;
	}
	
	public void addToNum(int pieceNo){
		this.pieceNo += pieceNo;
	}

	public Piece getPiece() {
		return piece;
	}
	
}
