public class Piece {

	private Colour colour;

	public Piece(Colour colour) {
		this.colour = colour;
	}

	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public String toString() {
		if (colour == Colour.Black) {
			return "B";
		}
		if (colour == Colour.White) {
				return "W";
			}
		return "0";
	}

}
